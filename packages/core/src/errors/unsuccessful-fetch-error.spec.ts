import { hacker, internet, random } from 'faker';
import 'isomorphic-fetch';

import {
  mapResponseToUnsucessfulFetchError,
  UnsucessfulFetchError
} from './unsuccessful-fetch-error';

describe('UnsucessfulFetchError', () => {
  it('should be a custom Error', () => {
    const url = internet.url();
    const status = random.number();
    const message = hacker.phrase();

    const error = new UnsucessfulFetchError({ url, status, message });

    expect(error).toBeInstanceOf(UnsucessfulFetchError);
    expect(error).toBeInstanceOf(Error);
    expect(error.name).toEqual(UnsucessfulFetchError.name);
    expect(error.url).toEqual(url);
    expect(error.status).toEqual(status);
    expect(error.message).toEqual(message);
  });
});

describe('mapResponseToUnsucessfulFetchError', () => {
  it('should create an UnsucessfulFetchError given a Response', () => {
    const url = internet.url();
    const status = random.number();
    const statusText = hacker.phrase();
    const body = hacker.noun();

    const response = new Response(body, { status, statusText });
    (response as any).url = url; // casting because url is constant

    const error = mapResponseToUnsucessfulFetchError(response);

    expect(error).toBeInstanceOf(UnsucessfulFetchError);
    expect(error.url).toEqual(url);
    expect(error.status).toEqual(status);
    expect(error.message).toEqual(statusText);
  });
});
