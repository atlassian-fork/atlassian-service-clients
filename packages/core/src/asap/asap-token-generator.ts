import { client } from 'jwt-authentication';

export type AsapClaims = {
  readonly issuer: string;
  readonly subject?: string;
  readonly audience: string;
  readonly keyId: string;
  readonly privateKey: string;
};

export function generateAsapToken({
  issuer,
  subject,
  audience,
  keyId,
  privateKey
}: AsapClaims): Promise<string> {
  return new Promise((resolve, reject) =>
    client
      .create()
      .generateToken(
        { 
          iss: issuer, 
          sub: subject ? subject : issuer, 
          aud: audience 
        },
        { kid: keyId, privateKey },
        (error, token) => {
          if (error) {
            reject(error);
          } else {
            resolve(token);
          }
        }
      )
  );
}
