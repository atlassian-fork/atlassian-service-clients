import { mapResponseToUnsucessfulFetchError } from '../errors';

export async function mapResponseToText(response: Response): Promise<string> {
  if (response.ok) {
    return await response.text();
  } else {
    throw mapResponseToUnsucessfulFetchError(response);
  }
}

export async function mapResponseToJson<T = any>(
  response: Response
): Promise<T> {
  if (response.ok) {
    return await response.json();
  } else {
    throw mapResponseToUnsucessfulFetchError(response);
  }
}
