import 'isomorphic-fetch';

export const PROD_BASE_URL =
  'https://geoip-service.internal.useast.atlassian.io';
export const STAGING_BASE_URL =
  'https://geoip-service.internal.useast.staging.atlassian.io';

export type GeoIPServiceClientOptions = {
  readonly baseUrl?: string;
};

const DEFAULT_OPTIONS = {};

export class GeoIPServiceClient {
  private readonly baseUrl: string;

  public constructor({
    baseUrl = STAGING_BASE_URL
  }: GeoIPServiceClientOptions = DEFAULT_OPTIONS) {
    this.baseUrl = baseUrl;
  }

  public async getLocation({
    ip
  }: GetLocationOptions): Promise<GetLocationResponse | null> {
    const response = await fetch(`${this.baseUrl}/location?ip=${ip}`);

    if (response.ok) {
      return await response.json();
    } else if (response.status === 404) {
      return null;
    } else {
      throw new Error(await response.text());
    }
  }
}

export type GetLocationOptions = {
  readonly ip: string;
};

export type GetLocationResponse = {
  readonly countryIsoCode: string;
  readonly countryName: string;
  readonly regionIsoCode: string;
  readonly regionName: string;
  readonly city: string;
  readonly latitude: number;
  readonly longitude: number;
};
