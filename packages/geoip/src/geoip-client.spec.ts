import fetchMock from 'fetch-mock';
import { NOT_FOUND, OK } from 'http-status-codes';

import { GeoIPServiceClient } from './geoip-client';

const baseUrl = 'http://base.url';

describe('GeoIPServiceClient`', () => {
  afterEach(() => fetchMock.reset());

  it('should have the ip in the GET request', async () => {
    const ip = '104.192.136.211';
    const client = new GeoIPServiceClient({ baseUrl });

    fetchMock.get(`begin:${baseUrl}`, {
      status: OK,
      body: {
        countryIsoCode: 'US',
        countryName: 'United States',
        regionIsoCode: 'TX',
        regionName: 'Texas',
        city: 'Austin',
        latitude: 30.2428,
        longitude: -97.7658
      }
    });

    await client.getLocation({ ip });

    expect(fetchMock.lastUrl()).toEqual(`${baseUrl}/location?ip=${ip}`);
  });

  it('should resolve to null given service responds with NOT_FOUND', async () => {
    const ip = '127.0.0.1';
    const client = new GeoIPServiceClient({ baseUrl });

    fetchMock.get(`begin:${baseUrl}`, {
      status: NOT_FOUND,
      body: 'null'
    });

    await expect(client.getLocation({ ip })).resolves.toBeNull();
  });
});
