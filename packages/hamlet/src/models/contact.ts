export type Contact = {
  readonly contactDetails: {
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly phone: string | null;
  };
  readonly organisationDetails: {
    readonly address1: string | null;
    readonly address2: string | null;
    readonly city: string | null;
    readonly state: string | null;
    readonly postcode: string | null;
    readonly taxId: string | null;
    readonly organisationName: string;
    readonly isoCountryCode: string | null;
    readonly country: string | null;
  };
};
