export type Cart = {
  readonly uuid: string;
  readonly created: string;
  readonly currency: string;
  readonly purchaseOrderNumber: string;
  readonly totalIncTax: number;
  readonly totalTax: number;
  readonly creditCardOnly: boolean;
  readonly isPayOnTermsEligible: boolean;
  readonly items: ReadonlyArray<CartItem>;
  readonly inEditMode: boolean;
  readonly promotionalCode: string;
  readonly hasTaxExemptRequest: boolean;
  readonly hasCoTermRequest: boolean;
  readonly showEnterpriseSupportAgreement: boolean;
  readonly showTrainingAgreement: boolean;
  readonly expertOrder: boolean;
  readonly marketplacePromotion?: MarketplacePromotion;
  readonly quoteNumber?: string;
};

export type CartItem = {
  readonly id: number;
  readonly orderableItemId: number;
  readonly description: string;
  readonly nodeCount: number;
  readonly maintenanceMonths: number;
  readonly maintenanceStartDate: Date;
  readonly maintenanceEndDate: Date;
  readonly listPrice: number;
  readonly discountAmount: number;
  readonly creditAmount: number;
  readonly totalTax: number;
  readonly totalIncTax: number;
  readonly remainingMaintenanceAmount: number;
  readonly usdMaintenancePortion: number;
  readonly autoRenewalEnabled: boolean;
  readonly productDetails: ProductDetails;
  readonly accountId?: number;
  readonly crossgradeAccountId?: number;
};

export type ProductDetails = {
  readonly productKey: string;
  readonly productDescription: string;
  readonly saleType: string;
  readonly licenseType: string;
  readonly unitCount: number;
  readonly editionDescription: string;
  readonly unitLabel: string;
  readonly marketplaceAddon: boolean;
  readonly atlassianAddon: boolean;
  readonly enterprise: boolean;
  readonly isSubscription: boolean;
  readonly isDataCenter: boolean;
  readonly isTraining: boolean;
};

export type MarketplacePromotion = {
  readonly code: string;
  readonly discountActive: boolean;
  readonly reason: string;
  readonly details: string;
};
