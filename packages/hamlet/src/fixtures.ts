import { random } from 'faker';
import fetchMock from 'fetch-mock';

import { isRecord, serializeCookie } from './utils';

export type SetupFixturesOptions = {
  readonly baseUrl: string;
};

export function setupFixtures({ baseUrl }: SetupFixturesOptions) {
  const xsrfToken = random.uuid();
  const cloudSessionTokenCookie = {
    name: 'cloud.session.token.stg',
    value: 'some-cloud-session-token-value'
  };

  fetchMock
    .get(
      (url, { headers }) => {
        if (isRecord(headers)) {
          return (
            compareUrl(`${baseUrl}/1.0/auth/xsrf/plain`)(url) &&
            headers.cookie === serializeCookie(cloudSessionTokenCookie)
          );
        } else {
          return false;
        }
      },
      {
        status: 200,
        body: xsrfToken
      }
    )
    .get(compareUrl(`${baseUrl}/1.0/auth/xsrf/plain`), {
      status: 403
    })
    .post(
      compareUrl(
        `${baseUrl}/1.0/public/aid/exists/existing-email@somewhere.com`
      ),
      {
        status: 200,
        body: {
          message: 'An Atlassian account exists for this email address',
          success: true
        }
      }
    )
    .post(url => url.startsWith(`${baseUrl}/1.0/public/aid/exists/`), {
      status: 200,
      body: {
        message: null,
        success: false
      }
    })
    .patch(url => url.startsWith(`${baseUrl}/1.0/public/contact/currentUser`), {
      status: 200,
      body: {
        contactDetails: {
          firstName: 'John',
          lastName: 'Wick',
          email: 'hireakiller@shhh.com',
          phone: '323-234-2352'
        }
      }
    });

  return {
    cloudSessionTokenCookie,
    xsrfToken
  };
}

export function cleanupFixtures() {
  return fetchMock.restore();
}

function compareUrl(url: string) {
  return (target: string) => target === url;
}
