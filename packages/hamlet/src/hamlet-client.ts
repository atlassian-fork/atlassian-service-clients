import 'isomorphic-fetch';

import {
  Cart,
  Contact,
  GetCartRequest,
  GetEmailExistsRequest,
  GetEmailExistsResponse,
  GetXsrfTokenRequest,
  PatchUserRequest,
  XsrfToken
} from './models';
import { mapResponseToJson, mapResponseToText, serializeCookie } from './utils';

export const DEFAULT_BASE_URL = 'https://wac.stg.internal.atlassian.com/hamlet';

export interface IHamletClient {
  getXsrfToken(request: GetXsrfTokenRequest): Promise<XsrfToken>;
  getEmailExists(
    request: GetEmailExistsRequest
  ): Promise<GetEmailExistsResponse>;
  getCart(request: GetCartRequest): Promise<Cart>;
  patchUser(request: PatchUserRequest): Promise<Contact>;
}

export type HamletClientOptions = {
  readonly baseUrl?: string;
};
export class HamletClient implements IHamletClient {
  private readonly baseUrl: string;

  constructor({ baseUrl = DEFAULT_BASE_URL }: HamletClientOptions = {}) {
    this.baseUrl = baseUrl;
  }

  async getXsrfToken({
    cloudSessionTokenCookie
  }: GetXsrfTokenRequest): Promise<XsrfToken> {
    return mapResponseToText(
      await fetch(`${this.baseUrl}/1.0/auth/xsrf/plain`, {
        headers: {
          cookie: serializeCookie(cloudSessionTokenCookie)
        }
      })
    );
  }

  async getEmailExists({
    email
  }: GetEmailExistsRequest): Promise<GetEmailExistsResponse> {
    return mapResponseToJson(
      await fetch(`${this.baseUrl}/1.0/public/aid/exists/${email}`, {
        method: 'POST'
      })
    );
  }

  async getCart({ cartId }: GetCartRequest): Promise<Cart> {
    return mapResponseToJson(
      await fetch(`${this.baseUrl}/1.0/public/order/cart/${cartId}`)
    );
  }

  async patchUser({
    cloudSessionTokenCookie,
    firstName,
    lastName
  }: PatchUserRequest): Promise<Contact> {
    const headers = new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    });
    headers.append(
      'ATL-XSRF-Token',
      await this.getXsrfToken({ cloudSessionTokenCookie })
    );
    headers.append('Cookie', serializeCookie(cloudSessionTokenCookie));
    return mapResponseToJson(
      await fetch(`${this.baseUrl}/1.0/public/contact/currentUser/patch`, {
        method: 'PATCH',
        headers,
        body: JSON.stringify([
          {
            op: 'replace',
            path: '/contactDetails/firstName',
            value: firstName
          },
          {
            op: 'replace',
            path: '/contactDetails/lastName',
            value: lastName
          }
        ])
      })
    );
  }
}
