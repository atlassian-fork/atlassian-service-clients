import { serialize } from 'cookie';

import { Cookie } from '../models';

export function serializeCookie({ name, value }: Cookie): string {
  return serialize(name, value);
}
