export const sampleProductEntriesResponse = {
  "sys": {
    "type": "Array"
  },
  "total": 1,
  "skip": 0,
  "limit": 100,
  "items": [
    {
      "fields": {
        "key": "jira-software.ondemand",
        "defaultStdPricingPlanMonthly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "6s4FTXxpbSAm7LZUSGIYQj",
            "type": "Entry",
            "createdAt": "2019-07-09T06:25:10.978Z",
            "updatedAt": "2019-07-09T12:06:02.688Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 4,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1200
                  },
                  {
                    "currency": "USD",
                    "unit_price": 10
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 10
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 800
                  },
                  {
                    "currency": "USD",
                    "unit_price": 7
                  }
                ],
                "starter": false,
                "pricing_strategy": "MARGINAL_PER_UNIT",
                "unit_description": "user",
                "unit_limit_lower": 11,
                "unit_limit_upper": 100
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 570
                  },
                  {
                    "currency": "USD",
                    "unit_price": 5
                  }
                ],
                "starter": false,
                "pricing_strategy": "MARGINAL_PER_UNIT",
                "unit_description": "user",
                "unit_limit_lower": 101,
                "unit_limit_upper": 250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 130
                  },
                  {
                    "currency": "USD",
                    "unit_price": 1.1
                  }
                ],
                "starter": false,
                "pricing_strategy": "MARGINAL_PER_UNIT",
                "unit_description": "user",
                "unit_limit_lower": 251,
                "unit_limit_upper": null
              }
            ],
            "pricingPlanId": "6cdfb69c-2250-4a27-a7e0-24beec0789aa",
            "sourceSequence": null,
            "source": "hams",
            "type": "Standard"
          }
        },
        "defaultStdPricingPlanYearly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "2qA56qJjyVIHXweVb43CM3",
            "type": "Entry",
            "createdAt": "2019-07-09T06:25:11.405Z",
            "updatedAt": "2019-07-09T12:06:03.037Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 4,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 12000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 100
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 10
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 119000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 1050
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 11,
                "unit_limit_upper": 15
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 199000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 1750
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 16,
                "unit_limit_upper": 25
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 397000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 3500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 26,
                "unit_limit_upper": 50
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 794000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 7000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 51,
                "unit_limit_upper": 100
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1360000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 12000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 101,
                "unit_limit_upper": 200
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1710000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 15050
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 201,
                "unit_limit_upper": 300
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1830000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 16150
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 301,
                "unit_limit_upper": 400
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1960000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 17250
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 401,
                "unit_limit_upper": 500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 2080000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 18350
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 501,
                "unit_limit_upper": 600
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 2330000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 20550
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 601,
                "unit_limit_upper": 800
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 2580000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 22750
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 801,
                "unit_limit_upper": 1000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 2830000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 24950
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1001,
                "unit_limit_upper": 1200
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 3080000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 27150
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1201,
                "unit_limit_upper": 1400
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 3330000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 29350
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1401,
                "unit_limit_upper": 1600
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 3580000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 31550
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1601,
                "unit_limit_upper": 1800
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 3830000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 33750
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1801,
                "unit_limit_upper": 2000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 4140000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 36500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2001,
                "unit_limit_upper": 2250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 4450000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 39250
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2251,
                "unit_limit_upper": 2500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 4760000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 42000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2501,
                "unit_limit_upper": 2750
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 5070000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 44750
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2751,
                "unit_limit_upper": 3000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 5390000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 47500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3001,
                "unit_limit_upper": 3250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 5700000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 50250
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3251,
                "unit_limit_upper": 3500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 6010000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 53000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3501,
                "unit_limit_upper": 3750
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 6320000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 55750
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3751,
                "unit_limit_upper": 4000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 6630000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 58500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4001,
                "unit_limit_upper": 4250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 6940000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 61250
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4251,
                "unit_limit_upper": 4500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 7260000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 64000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4501,
                "unit_limit_upper": 4750
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 7570000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 66750
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4751,
                "unit_limit_upper": 5000
              }
            ],
            "pricingPlanId": "767abfa1-0477-4016-aad4-aea22ba2917c",
            "sourceSequence": null,
            "source": "hams",
            "type": "Standard"
          }
        },
        "defaultPremiumPricingPlanMonthly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "2zi72Wig8w7huYSKH1g68K",
            "type": "Entry",
            "createdAt": "2019-07-09T06:25:11.844Z",
            "updatedAt": "2019-07-09T12:06:00.502Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 2,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1590
                  },
                  {
                    "currency": "USD",
                    "unit_price": 14
                  }
                ],
                "starter": false,
                "pricing_strategy": "MARGINAL_PER_UNIT",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 100
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1140
                  },
                  {
                    "currency": "USD",
                    "unit_price": 10
                  }
                ],
                "starter": false,
                "pricing_strategy": "MARGINAL_PER_UNIT",
                "unit_description": "user",
                "unit_limit_lower": 101,
                "unit_limit_upper": 250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 800
                  },
                  {
                    "currency": "USD",
                    "unit_price": 7
                  }
                ],
                "starter": false,
                "pricing_strategy": "MARGINAL_PER_UNIT",
                "unit_description": "user",
                "unit_limit_lower": 251,
                "unit_limit_upper": null
              }
            ],
            "pricingPlanId": "028c2ae1-faf8-46c8-ab64-88cbf8f51555",
            "sourceSequence": null,
            "source": "hams",
            "type": "Premium"
          }
        },
        "defaultPremiumPricingPlanYearly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "1XWSFDbZvUYAMlho7iaFrP",
            "type": "Entry",
            "createdAt": "2019-07-09T06:25:12.294Z",
            "updatedAt": "2019-07-09T12:06:01.027Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 2,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 159000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 1400
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 10
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 238000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 2100
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 11,
                "unit_limit_upper": 15
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 397000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 3500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 16,
                "unit_limit_upper": 25
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 794000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 7000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 26,
                "unit_limit_upper": 50
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1590000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 14000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 51,
                "unit_limit_upper": 100
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 2720000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 24000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 101,
                "unit_limit_upper": 200
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 3690000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 32500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 201,
                "unit_limit_upper": 300
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 4480000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 39500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 301,
                "unit_limit_upper": 400
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 5270000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 46500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 401,
                "unit_limit_upper": 500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 6070000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 53500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 501,
                "unit_limit_upper": 600
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 7650000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 67500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 601,
                "unit_limit_upper": 800
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 9240000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 81500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 801,
                "unit_limit_upper": 1000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 10900000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 95500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1001,
                "unit_limit_upper": 1200
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 12500000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 109500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1201,
                "unit_limit_upper": 1400
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 14000000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 123500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1401,
                "unit_limit_upper": 1600
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 15600000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 137500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1601,
                "unit_limit_upper": 1800
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 17200000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 151500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 1801,
                "unit_limit_upper": 2000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 19200000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 169000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2001,
                "unit_limit_upper": 2250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 21200000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 186500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2251,
                "unit_limit_upper": 2500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 23200000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 204000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2501,
                "unit_limit_upper": 2750
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 25100000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 221500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2751,
                "unit_limit_upper": 3000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 27100000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 239000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3001,
                "unit_limit_upper": 3250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 29100000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 256500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3251,
                "unit_limit_upper": 3500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 31100000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 274000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3501,
                "unit_limit_upper": 3750
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 33100000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 291500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 3751,
                "unit_limit_upper": 4000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 35100000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 309000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4001,
                "unit_limit_upper": 4250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 37000000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 326500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4251,
                "unit_limit_upper": 4500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 39000000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 344000
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4501,
                "unit_limit_upper": 4750
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 41000000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 361500
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 4751,
                "unit_limit_upper": 5000
              }
            ],
            "pricingPlanId": "bf27d36c-1b44-4e45-8071-809fc0153909",
            "sourceSequence": null,
            "source": "hams",
            "type": "Premium"
          }
        },
        "defaultFreePricingPlanMonthly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "6YLPNIR9mjXKvQIKL7AocI",
            "type": "Entry",
            "createdAt": "2019-07-09T06:25:12.699Z",
            "updatedAt": "2019-07-09T12:06:01.660Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 2,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 0
                  },
                  {
                    "currency": "USD",
                    "unit_price": 0
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 10
              }
            ],
            "pricingPlanId": "833a8507-a22f-4fe9-b371-d08f8f154a4b",
            "sourceSequence": null,
            "source": "hams",
            "type": "Free"
          }
        }
      },
      "sys": {
        "space": {
          "sys": {
            "type": "Link",
            "linkType": "Space",
            "id": "3s3v3nq72la0"
          }
        },
        "id": "1B7iUzoOc7C2xzBPYDgm37",
        "type": "Entry",
        "createdAt": "2019-07-08T20:38:28.429Z",
        "updatedAt": "2019-07-09T12:06:03.364Z",
        "environment": {
          "sys": {
            "id": "stg",
            "type": "Link",
            "linkType": "Environment"
          }
        },
        "revision": 5,
        "contentType": {
          "sys": {
            "type": "Link",
            "linkType": "ContentType",
            "id": "product"
          }
        },
        "locale": "en-US"
      }
    }
  ],
  "includes": {
    "Entry": [
      {
        "sys": {
          "space": {
            "sys": {
              "type": "Link",
              "linkType": "Space",
              "id": "3s3v3nq72la0"
            }
          },
          "id": "1XWSFDbZvUYAMlho7iaFrP",
          "type": "Entry",
          "createdAt": "2019-07-09T06:25:12.294Z",
          "updatedAt": "2019-07-09T12:06:01.027Z",
          "environment": {
            "sys": {
              "id": "stg",
              "type": "Link",
              "linkType": "Environment"
            }
          },
          "revision": 2,
          "contentType": {
            "sys": {
              "type": "Link",
              "linkType": "ContentType",
              "id": "pricingPlan"
            }
          },
          "locale": "en-US"
        },
        "fields": {
          "status": "ACTIVE",
          "configuration": [
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 159000
                },
                {
                  "currency": "USD",
                  "unit_price": 1400
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 0,
              "unit_limit_upper": 10
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 238000
                },
                {
                  "currency": "USD",
                  "unit_price": 2100
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 11,
              "unit_limit_upper": 15
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 397000
                },
                {
                  "currency": "USD",
                  "unit_price": 3500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 16,
              "unit_limit_upper": 25
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 794000
                },
                {
                  "currency": "USD",
                  "unit_price": 7000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 26,
              "unit_limit_upper": 50
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1590000
                },
                {
                  "currency": "USD",
                  "unit_price": 14000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 51,
              "unit_limit_upper": 100
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 2720000
                },
                {
                  "currency": "USD",
                  "unit_price": 24000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 101,
              "unit_limit_upper": 200
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 3690000
                },
                {
                  "currency": "USD",
                  "unit_price": 32500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 201,
              "unit_limit_upper": 300
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 4480000
                },
                {
                  "currency": "USD",
                  "unit_price": 39500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 301,
              "unit_limit_upper": 400
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 5270000
                },
                {
                  "currency": "USD",
                  "unit_price": 46500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 401,
              "unit_limit_upper": 500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 6070000
                },
                {
                  "currency": "USD",
                  "unit_price": 53500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 501,
              "unit_limit_upper": 600
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 7650000
                },
                {
                  "currency": "USD",
                  "unit_price": 67500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 601,
              "unit_limit_upper": 800
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 9240000
                },
                {
                  "currency": "USD",
                  "unit_price": 81500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 801,
              "unit_limit_upper": 1000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 10900000
                },
                {
                  "currency": "USD",
                  "unit_price": 95500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1001,
              "unit_limit_upper": 1200
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 12500000
                },
                {
                  "currency": "USD",
                  "unit_price": 109500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1201,
              "unit_limit_upper": 1400
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 14000000
                },
                {
                  "currency": "USD",
                  "unit_price": 123500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1401,
              "unit_limit_upper": 1600
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 15600000
                },
                {
                  "currency": "USD",
                  "unit_price": 137500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1601,
              "unit_limit_upper": 1800
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 17200000
                },
                {
                  "currency": "USD",
                  "unit_price": 151500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1801,
              "unit_limit_upper": 2000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 19200000
                },
                {
                  "currency": "USD",
                  "unit_price": 169000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2001,
              "unit_limit_upper": 2250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 21200000
                },
                {
                  "currency": "USD",
                  "unit_price": 186500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2251,
              "unit_limit_upper": 2500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 23200000
                },
                {
                  "currency": "USD",
                  "unit_price": 204000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2501,
              "unit_limit_upper": 2750
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 25100000
                },
                {
                  "currency": "USD",
                  "unit_price": 221500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2751,
              "unit_limit_upper": 3000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 27100000
                },
                {
                  "currency": "USD",
                  "unit_price": 239000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3001,
              "unit_limit_upper": 3250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 29100000
                },
                {
                  "currency": "USD",
                  "unit_price": 256500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3251,
              "unit_limit_upper": 3500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 31100000
                },
                {
                  "currency": "USD",
                  "unit_price": 274000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3501,
              "unit_limit_upper": 3750
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 33100000
                },
                {
                  "currency": "USD",
                  "unit_price": 291500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3751,
              "unit_limit_upper": 4000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 35100000
                },
                {
                  "currency": "USD",
                  "unit_price": 309000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4001,
              "unit_limit_upper": 4250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 37000000
                },
                {
                  "currency": "USD",
                  "unit_price": 326500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4251,
              "unit_limit_upper": 4500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 39000000
                },
                {
                  "currency": "USD",
                  "unit_price": 344000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4501,
              "unit_limit_upper": 4750
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 41000000
                },
                {
                  "currency": "USD",
                  "unit_price": 361500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4751,
              "unit_limit_upper": 5000
            }
          ],
          "pricingPlanId": "bf27d36c-1b44-4e45-8071-809fc0153909",
          "sourceSequence": null,
          "source": "hams",
          "type": "Premium"
        }
      },
      {
        "sys": {
          "space": {
            "sys": {
              "type": "Link",
              "linkType": "Space",
              "id": "3s3v3nq72la0"
            }
          },
          "id": "2qA56qJjyVIHXweVb43CM3",
          "type": "Entry",
          "createdAt": "2019-07-09T06:25:11.405Z",
          "updatedAt": "2019-07-09T12:06:03.037Z",
          "environment": {
            "sys": {
              "id": "stg",
              "type": "Link",
              "linkType": "Environment"
            }
          },
          "revision": 4,
          "contentType": {
            "sys": {
              "type": "Link",
              "linkType": "ContentType",
              "id": "pricingPlan"
            }
          },
          "locale": "en-US"
        },
        "fields": {
          "status": "ACTIVE",
          "configuration": [
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 12000
                },
                {
                  "currency": "USD",
                  "unit_price": 100
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 0,
              "unit_limit_upper": 10
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 119000
                },
                {
                  "currency": "USD",
                  "unit_price": 1050
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 11,
              "unit_limit_upper": 15
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 199000
                },
                {
                  "currency": "USD",
                  "unit_price": 1750
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 16,
              "unit_limit_upper": 25
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 397000
                },
                {
                  "currency": "USD",
                  "unit_price": 3500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 26,
              "unit_limit_upper": 50
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 794000
                },
                {
                  "currency": "USD",
                  "unit_price": 7000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 51,
              "unit_limit_upper": 100
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1360000
                },
                {
                  "currency": "USD",
                  "unit_price": 12000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 101,
              "unit_limit_upper": 200
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1710000
                },
                {
                  "currency": "USD",
                  "unit_price": 15050
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 201,
              "unit_limit_upper": 300
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1830000
                },
                {
                  "currency": "USD",
                  "unit_price": 16150
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 301,
              "unit_limit_upper": 400
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1960000
                },
                {
                  "currency": "USD",
                  "unit_price": 17250
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 401,
              "unit_limit_upper": 500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 2080000
                },
                {
                  "currency": "USD",
                  "unit_price": 18350
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 501,
              "unit_limit_upper": 600
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 2330000
                },
                {
                  "currency": "USD",
                  "unit_price": 20550
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 601,
              "unit_limit_upper": 800
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 2580000
                },
                {
                  "currency": "USD",
                  "unit_price": 22750
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 801,
              "unit_limit_upper": 1000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 2830000
                },
                {
                  "currency": "USD",
                  "unit_price": 24950
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1001,
              "unit_limit_upper": 1200
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 3080000
                },
                {
                  "currency": "USD",
                  "unit_price": 27150
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1201,
              "unit_limit_upper": 1400
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 3330000
                },
                {
                  "currency": "USD",
                  "unit_price": 29350
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1401,
              "unit_limit_upper": 1600
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 3580000
                },
                {
                  "currency": "USD",
                  "unit_price": 31550
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1601,
              "unit_limit_upper": 1800
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 3830000
                },
                {
                  "currency": "USD",
                  "unit_price": 33750
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 1801,
              "unit_limit_upper": 2000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 4140000
                },
                {
                  "currency": "USD",
                  "unit_price": 36500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2001,
              "unit_limit_upper": 2250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 4450000
                },
                {
                  "currency": "USD",
                  "unit_price": 39250
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2251,
              "unit_limit_upper": 2500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 4760000
                },
                {
                  "currency": "USD",
                  "unit_price": 42000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2501,
              "unit_limit_upper": 2750
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 5070000
                },
                {
                  "currency": "USD",
                  "unit_price": 44750
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2751,
              "unit_limit_upper": 3000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 5390000
                },
                {
                  "currency": "USD",
                  "unit_price": 47500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3001,
              "unit_limit_upper": 3250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 5700000
                },
                {
                  "currency": "USD",
                  "unit_price": 50250
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3251,
              "unit_limit_upper": 3500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 6010000
                },
                {
                  "currency": "USD",
                  "unit_price": 53000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3501,
              "unit_limit_upper": 3750
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 6320000
                },
                {
                  "currency": "USD",
                  "unit_price": 55750
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 3751,
              "unit_limit_upper": 4000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 6630000
                },
                {
                  "currency": "USD",
                  "unit_price": 58500
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4001,
              "unit_limit_upper": 4250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 6940000
                },
                {
                  "currency": "USD",
                  "unit_price": 61250
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4251,
              "unit_limit_upper": 4500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 7260000
                },
                {
                  "currency": "USD",
                  "unit_price": 64000
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4501,
              "unit_limit_upper": 4750
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 7570000
                },
                {
                  "currency": "USD",
                  "unit_price": 66750
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 4751,
              "unit_limit_upper": 5000
            }
          ],
          "pricingPlanId": "767abfa1-0477-4016-aad4-aea22ba2917c",
          "sourceSequence": null,
          "source": "hams",
          "type": "Standard"
        }
      },
      {
        "sys": {
          "space": {
            "sys": {
              "type": "Link",
              "linkType": "Space",
              "id": "3s3v3nq72la0"
            }
          },
          "id": "2zi72Wig8w7huYSKH1g68K",
          "type": "Entry",
          "createdAt": "2019-07-09T06:25:11.844Z",
          "updatedAt": "2019-07-09T12:06:00.502Z",
          "environment": {
            "sys": {
              "id": "stg",
              "type": "Link",
              "linkType": "Environment"
            }
          },
          "revision": 2,
          "contentType": {
            "sys": {
              "type": "Link",
              "linkType": "ContentType",
              "id": "pricingPlan"
            }
          },
          "locale": "en-US"
        },
        "fields": {
          "status": "ACTIVE",
          "configuration": [
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1590
                },
                {
                  "currency": "USD",
                  "unit_price": 14
                }
              ],
              "starter": false,
              "pricing_strategy": "MARGINAL_PER_UNIT",
              "unit_description": "user",
              "unit_limit_lower": 0,
              "unit_limit_upper": 100
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1140
                },
                {
                  "currency": "USD",
                  "unit_price": 10
                }
              ],
              "starter": false,
              "pricing_strategy": "MARGINAL_PER_UNIT",
              "unit_description": "user",
              "unit_limit_lower": 101,
              "unit_limit_upper": 250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 800
                },
                {
                  "currency": "USD",
                  "unit_price": 7
                }
              ],
              "starter": false,
              "pricing_strategy": "MARGINAL_PER_UNIT",
              "unit_description": "user",
              "unit_limit_lower": 251,
              "unit_limit_upper": null
            }
          ],
          "pricingPlanId": "028c2ae1-faf8-46c8-ab64-88cbf8f51555",
          "sourceSequence": null,
          "source": "hams",
          "type": "Premium"
        }
      },
      {
        "sys": {
          "space": {
            "sys": {
              "type": "Link",
              "linkType": "Space",
              "id": "3s3v3nq72la0"
            }
          },
          "id": "6YLPNIR9mjXKvQIKL7AocI",
          "type": "Entry",
          "createdAt": "2019-07-09T06:25:12.699Z",
          "updatedAt": "2019-07-09T12:06:01.660Z",
          "environment": {
            "sys": {
              "id": "stg",
              "type": "Link",
              "linkType": "Environment"
            }
          },
          "revision": 2,
          "contentType": {
            "sys": {
              "type": "Link",
              "linkType": "ContentType",
              "id": "pricingPlan"
            }
          },
          "locale": "en-US"
        },
        "fields": {
          "status": "ACTIVE",
          "configuration": [
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 0
                },
                {
                  "currency": "USD",
                  "unit_price": 0
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 0,
              "unit_limit_upper": 10
            }
          ],
          "pricingPlanId": "833a8507-a22f-4fe9-b371-d08f8f154a4b",
          "sourceSequence": null,
          "source": "hams",
          "type": "Free"
        }
      },
      {
        "sys": {
          "space": {
            "sys": {
              "type": "Link",
              "linkType": "Space",
              "id": "3s3v3nq72la0"
            }
          },
          "id": "6s4FTXxpbSAm7LZUSGIYQj",
          "type": "Entry",
          "createdAt": "2019-07-09T06:25:10.978Z",
          "updatedAt": "2019-07-09T12:06:02.688Z",
          "environment": {
            "sys": {
              "id": "stg",
              "type": "Link",
              "linkType": "Environment"
            }
          },
          "revision": 4,
          "contentType": {
            "sys": {
              "type": "Link",
              "linkType": "ContentType",
              "id": "pricingPlan"
            }
          },
          "locale": "en-US"
        },
        "fields": {
          "status": "ACTIVE",
          "configuration": [
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1200
                },
                {
                  "currency": "USD",
                  "unit_price": 10
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 0,
              "unit_limit_upper": 10
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 800
                },
                {
                  "currency": "USD",
                  "unit_price": 7
                }
              ],
              "starter": false,
              "pricing_strategy": "MARGINAL_PER_UNIT",
              "unit_description": "user",
              "unit_limit_lower": 11,
              "unit_limit_upper": 100
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 570
                },
                {
                  "currency": "USD",
                  "unit_price": 5
                }
              ],
              "starter": false,
              "pricing_strategy": "MARGINAL_PER_UNIT",
              "unit_description": "user",
              "unit_limit_lower": 101,
              "unit_limit_upper": 250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 130
                },
                {
                  "currency": "USD",
                  "unit_price": 1.1
                }
              ],
              "starter": false,
              "pricing_strategy": "MARGINAL_PER_UNIT",
              "unit_description": "user",
              "unit_limit_lower": 251,
              "unit_limit_upper": null
            }
          ],
          "pricingPlanId": "6cdfb69c-2250-4a27-a7e0-24beec0789aa",
          "sourceSequence": null,
          "source": "hams",
          "type": "Standard"
        }
      }
    ]
  }
};
