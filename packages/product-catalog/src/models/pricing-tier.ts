import { PricingStrategy } from './pricing-strategy';

export type PricingTier = {
  readonly starter: boolean;
  readonly pricingStrategy: PricingStrategy;
  readonly unitDescription: string;
  readonly unitLimitLower: number | null;
  readonly unitLimitUpper: number | null;
  readonly prices: ReadonlyArray<PricingTierPrice>;
};

export type PricingTierPrice = {
  readonly currency: string;
  readonly unitPrice: number;
};
