import { randomBytes } from 'crypto';
import { sign } from 'jsonwebtoken';
import { GenerateAsapTokenOptions } from './asap.types'

export const DEFAULT_EXPIRES_IN_SECONDS = 60;

export function generateAsapToken({
  payload = {},
  privateKey,
  keyId,
  expiresIn = DEFAULT_EXPIRES_IN_SECONDS,
  audience,
  issuer,
  subject = issuer
}: GenerateAsapTokenOptions): string {
  return sign(payload, privateKey, {
    algorithm: 'RS256',
    keyid: keyId,
    expiresIn,
    audience,
    issuer,
    subject,
    jwtid: randomBytes(20).toString('hex')
  });
}
