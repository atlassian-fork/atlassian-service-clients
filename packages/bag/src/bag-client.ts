import {
  generateAsapToken,
  mapResponseToJson
} from '@atlassian/asc-core';

import {
  BagClientOptions,
  GetEntitlementGroupRequest,
  GetEntitlementGroupResponse,
  isAsapAuth
} from './bag-client.types';


export const PROD_BASE_URL = 'https://billing-api-gateway.prod.atl-paas.net';
export const STAGING_BASE_URL = 'https://billing-api-gateway.staging.atl-paas.net';
const AUDIENCE = 'billing-api-gateway';


export class BagClient {
  private readonly baseUrl: string;

  public constructor({ baseUrl = STAGING_BASE_URL }: BagClientOptions = {}) {
    this.baseUrl = baseUrl;
  }

  public async getEntitlement(
    request: GetEntitlementGroupRequest
  ): Promise<GetEntitlementGroupResponse> {
    const headers = new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    });

    const { auth, entitlementGroupId } = request;

    if (isAsapAuth(auth)) {
      headers.append(
        'Authorization',
        `Bearer ${await generateAsapToken({
          ...auth.asap,
          audience: AUDIENCE
        })}`
      );
    }

    return mapResponseToJson(
      await fetch(`${this.baseUrl}/api/entitlement-groups/${entitlementGroupId}`, {
        method: 'GET',
        headers
      })
    );
  }
}
