import * as core from '@atlassian/asc-core';
import fetchMock from 'fetch-mock';
import { BAD_REQUEST, OK } from 'http-status-codes';
import 'isomorphic-fetch';
import { BagClient, STAGING_BASE_URL } from './index';

const asap = {
  issuer: 'some-issuer',
  keyId: 'some-key-id',
  privateKey: 'some-private-key'
};
const asapToken = 'some-asap-token';
const baseUrl = 'http://base.url';


describe('BagClient', () => {
  describe('getEntitlement', () => {
    const entitlementGroupId = 'some-cloud-id';

    const responseBody = {
      id: "some-id",
      name: "some-name",
      sen: "SEN-7676843",
      entitlements: [
        {
          id: "7676844",
          sen: "SEN-7676844",
          productKey: "jira-software.ondemand",
          name: "Jira Software (Cloud)",
          entitlementGroupId: "some-entitlement-group-id",
          accountId: "some-account-id",
          startDate: "2028-09-14",
          endDate: "2028-09-14",
          creationDate: "2018-09-14",
          currentEdition: "standard",
          trialEditionEndDate: "2028-10-14",
          trialEdition: "premium",
          futureEdition: "standard",
          editionTransitions: [
            "DOWNGRADE"
          ],
          status: "ACTIVE",
          suspended: false,
          entitlementMigrationEvaluation: {
            usageLimit: 0
          }
        }
      ],
      accountId: "some-account-id",
      status: "ACTIVE",
      suspended: false,
      currentBillingPeriod: {
          startDate: "2019-06-02",
          endDate: "2028-09-14",
          renewalFrequency: "MONTHLY"
      },
      nextBillingPeriod: {
          startDate: "2028-09-14",
          endDate: "2028-10-14",
          renewalFrequency: "MONTHLY"
      },
      autoRenewed: true,
      renewalAction: "AUTO_RENEW",
      license: "COMMERCIAL"
    };

    beforeEach(() =>
      jest
        .spyOn(core, 'generateAsapToken')
        .mockResolvedValue(asapToken));

    afterEach(() => {
      jest.restoreAllMocks();
      fetchMock.restore();
    });

    it('should return entitlement groups by entitlement group ID', async () => {
      const client = new BagClient();

      fetchMock.get(`begin:${STAGING_BASE_URL}`, {
        status: OK, 
        body: responseBody
      });

      await expect(
        client.getEntitlement({
          entitlementGroupId,
          auth: { asap }
        })
      ).resolves.toEqual(responseBody);

      expect(true).toBe(true);
    });

    it('should reject with error given BAD_REQUEST status', async () => {
      const client = new BagClient({ baseUrl });

      fetchMock.get(`begin:${baseUrl}`, { status: BAD_REQUEST });

      await expect(
        client.getEntitlement({
          entitlementGroupId,
          auth: { asap }
        })
      ).rejects.toMatchObject({
        url: `${baseUrl}/api/entitlement-groups/${entitlementGroupId}`,
        status: BAD_REQUEST,
        message: 'Bad Request'
      });
    });
  });
});